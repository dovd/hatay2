//var route = 'http://localhost:3000'
var route = 'http://35.197.138.75:8080'
module.exports = {
    WINDOW: {
        MAIN: {
            width: 1200,
            height: 800
        },
        USER_NOMAL: {
            width: 1200,
            height: 800
        },
        USER_SYNC: {
            width: 580,
            height: 530 + 70,
            TIME_WAIT: 7000,
            TIME_COUNT: 10,
            SHOW: false
        },
        USER_TEST: {
            width: 300,
            height: 300
        }
    },
    ROUTE_OPEN_LANDING_PAGE: 'https://shopeelike.com',
    ROUTE_INDEX: `file://${__dirname}/view/index.html`,
    STYLE_THEME: `${__dirname}/styles/theme.css`,
    STYLE_STYLE: `${__dirname}/styles/style.css`,
    TOKEN_CODE: 'SPC_CDS',
    LINK_ORDER_WAIT: 'https://banhang.shopee.vn/api/v2/orders/?is_massship=false&limit=4000&offset=0&search=&type=unpaid&SPC_CDS_VER=2',
    LINK_CHECK_LOGIN: 'https://banhang.shopee.vn/api/v1/login/?SPC_CDS_VER=2',
    MAX_PRODUCT_PUSH: 5,
    LS: {
        USERNAME: 'fasjdflkasfhkaslddal',
        PASSWORD: 'dashgdhksgfhjkdsgfjs',
        LIST: 'diasgdasdgajsgdsa',
        OUTDATE: 'dsajkbkajsdvhjasd',
        OUTDATE_SHOPEELIKE: 'dasjhdhkasjdgkagdjsaasasassa',
        VIEW_USER: 'dhuwiedhiwdhiwhjs',
        CONFIG: 'bkhasdhaksfhdsbfdsjbfsad',
        LAST_USE: 'hbdkabkeiewisadsdsdsddsadd',
        TOKEN:'token',
        DEFAULT_SHOP_ID: 'default_shop_id',
        POINT: '0',
        EXCHANGES:[]
    },
    API: {
    	BASE:route +'/api',
        LOGIN: route + '/login',
        CONFIG: route + '/client_config.json'
    },
    NOTI: {
        DEFAULT_ERR: 'Opp! Có lỗi gì đó?',
        DEFAULT_NOTI: 'Thông báo',
        LOGIN_FAIL: 'Đăng nhập thất bại, máy chủ ShopeeLike đang bận hoặc máy tính của bạn không thể kết nối Internet',
        USERNAME_PASSWORD_REQUIRE: 'Username hoặc password không được để trống',
        NOT_FOUND_USER: 'Username hoặc password không đúng!',
        NOTTDC: 'Tài khoản này không phải tài khoản Tốc Độ Cao',
        NEW_USER_WRONG_FORMAT: 'Username Shopee này chưa đúng định dạng',
        LOADING_SERVER: 'Đang kết nối tới máy chủ của ShopeeLike...',
        LOADING_WAIT_CREATE_USER: 'Đang chờ bạn đăng nhập và khởi tạo user Shopee mới...',
        LOADING_WAIT_SETTING_PUSH: 'Đang chờ bạn hoàn thành cài đặt đẩy sản phẩm...',
        NEW_USER_EXIST: 'User Shopee này đã tồn tại trong danh sách quản lý của bạn.',
        ALERT_CLOSE_LOGIN_WINDOW: 'Chưa liên kết được shop của bạn bạn, bạn có muốn huỷ cài đặt Shop không?',
        ALERT_CLOSE_ALL_WINDOW: 'Bạn muốn thoát hoàn toàn?',
        CONFIRM: 'Xác nhận',
        DELETE_USER_CONFIRM_TEXT: 'Bạn có chắc chắn muốn đăng xuất tài khoản Shopee này khỏi trình quản lý?',
        YES: 'Có',
        NO: 'Không',
        MAX_PUSH: 'Chỉ được chọn đẩy tự động tối đa 5 sản phẩm',
        PUSH_EXIST: 'Đã tồn tại sản phẩm này trong danh mục đẩy tự động',
        PUSH_TIMER_HOUR_IN_CORRECT: 'Giờ không đúng định dạng, bạn chỉ được nhập từ 0 đến 23 giờ',
        PUSH_TIMER_MINUTE_IN_CORRECT: 'Phút không đúng định dạng, bạn chỉ được nhập từ 0 đến 59 phút',
        PUSH_TIMER_LIST_IN_CORRECT: 'Lịch hẹn giờ phải hơn kém nhau 4 tiếng, kiểm tra lại danh sách hẹn giờ của sản phẩm này.',
        PUSH_TIMER_LIST_MAX: 'Chỉ hẹn giờ được tối đa 6 lần 1 ngày / 1 sản phẩm',
        MAX_USER_MANAGER: 'Không thể thêm tài khoản quản lý vì bạn đã đạt đến giới hạn tài khoản quản lý cho phép',
        SLOGAN: 'Ready to scale your business!',
        LOW_VERSION: 'Phiên bản hiện tại đã quá cũ! Hãy vào trang chủ của Shopeelike.com và cập nhật ngay!',
        CONNECT_AFTER: 'Không thể kết nối tới máy chủ, kết nối lại sau ',
        DOWNLOAD_EXCEL_SUCCESS: 'Tải xuống file excel thành công, file được lưu trong thư mục Downloads của bạn',
        DOWNLOAD_EXCEL_EMPTY: 'Bạn phải chọn các sản phẩm bạn muốn tải xuống',
        ACCOUNT_LITMIT: 'Đã hết lượt theo dõi xin vui lòng thử lại sau.',
        STOP_EXCHANGE_TITLE: 'Dừng auto like',
        STOP_EXCHANGE_CONTENT: 'Bạn có muốn dừng auto like?',
        REGISTER_TITLE_SUCCESS: 'Đăng ký thành công',
        REGISTER_CONTENT_SUCCESS: 'Đăng ký thành công bạn có 1 ngày để dùng thử.',
        SET_DEFAULT_ID: 'Mỗi tài khoản chỉ được tăng like cho 2 tài khoản shopee cùng 1 thời điểm.',
        SET_DEFAULT_ID_SUCCESS: 'Set tài khoản tăng like thành công',
        SET_DEFAULT_ID_CONTENT: 'Bạn có muốn set tài khoản này làm tài khoản tăng like'
    },
    ERR: {
        SERVER_FAIL: 'SERVER_FAIL',
        NOT_LOGIN: 'NOT_LOGIN',
        USERNAME_REQUIRE: 'username_require',
        PASSWORD_REQUIRE: 'password_require',
        NOT_FOUND_USER: 'not_found',
        NOTTDC: 'NOTTDC',
        NEW_USER_WRONG_FORMAT: 'NEW_USER_WRONG_FORMAT',
        NEW_USER_EXIST: 'NEW_USER_EXIST',
        MAX_PUSH: 'MAX_PUSH',
        PUSH_EXIST: 'PUSH_EXIST',
        PUSH_TIMER_HOUR_IN_CORRECT: 'PUSH_TIMER_HOUR_IN_CORRECT',
        PUSH_TIMER_MINUTE_IN_CORRECT: 'PUSH_TIMER_MINUTE_IN_CORRECT',
        PUSH_TIMER_LIST_IN_CORRECT: 'PUSH_TIMER_LIST_IN_CORRECT',
        PUSH_TIMER_LIST_MAX: 'PUSH_TIMER_LIST_MAX',
        MAX_USER_MANAGER: 'MAX_USER_MANAGER',
        LOW_VERSION: 'LOW_VERSION',
        DOWNLOAD_EXCEL_EMPTY: 'DOWNLOAD_EXCEL_EMPTY'
    },
    DOM: {
        MODAL_ENTER_ACC: '#modal-enter-new-acc'
    },
    SHOPEE: {
    	HOME: 'https://shopee.vn/',
    	SEARCH_ITEM:'https://shopee.vn/api/v2/search_items/?by=pop&order=desc&page_type=shop',
    	LIKE_ITEM:'https://shopee.vn/api/v0/buyer/like/shop/',
    	FLOW_SHOP:'https://shopee.vn/api/v0/buyer/follow/shop/',
        KENH_NGUOI_BAN: 'https://banhang.shopee.vn',
        KENH_NGUOI_BAN_ORDER: 'https://banhang.shopee.vn/portal/sale?type=toship',
        CHECK_LOGIN: 'https://banhang.shopee.vn/api/v1/login/?SPC_CDS_VER=2',
        GET_INFO: 'https://banhang.shopee.vn/api/v2/users/',
        GET_ORDER:'https://banhang.shopee.vn/api/v2/orders/?is_massship=false&search=&type=toship&SPC_CDS_VER=2',
        SEARCH_PRODUCT: 'https://banhang.shopee.vn/api/v2/products/?limit=24&offset=0&order=list_time_dsc&type=active',
        GET_ALL_PRODUCT: 'https://banhang.shopee.vn/api/v2/products/?order=list_time_dsc&type=active',
        DEFAULT_AVATAR:'https://cdngarenanow-a.akamaihd.net/shopee/shopee-seller-live-vn/images/default-avatar.png',
        GET_IDS_BY_USERNAME: 'https://shopee.vn/api/v1/shop_ids_by_username/',
        SHOP_INFO:'https://shopee.vn/api/v1/shops/'
    },
    SHOPEE_DOM: {
        CHAT: '.shopee-chat-container .badge.active',
        CHAT_OPEN: '.shopee-chat-container *',
        CHAT_CLOSE: '.shopee-chat-close-button *',
        CHAT_CLOSE_BTN: '.shopee-chat-close-button'
    },
    GO: {
        LOGIN: '/controller/login.js',
        SYNC: '/controller/sync.js',
        PUSH_MANAGER: '/controller/push-manager.js',
        GO: '/controller/go.js',
        PRINT: '/controller/print.js',
        LIKE: '/controller/like.js',
    },
    STATUS: {
    	STOP_NAME: 'Dừng',
    	RUNNING_NAME: 'Đang chạy',
    	STOPING_NAME: 'Đang Dừng',
    	WAIT_NAME: 'Đang tìm shop',
    	WAIT: '3',
    	STOP: '0',
    	RUNNING: '1',
    	STOPING: '2',
    },
    IPC: {
        LOGIN_SUCCESS: 'LOGIN_SUCCESS',
        LOGIN_CLOSE: 'LOGIN_CLOSE',
        SYNC_SUCCESS: 'SYNC_SUCCESS',
        SYNC_FAIL: 'SYNC_FAIL',
        SYNC_NOT_LOGIN: 'SYNC_NOT_LOGIN',
        CALL_GET_PRODUCT: 'CALL_GET_PRODUCT',
        PUSH_READY_TO_USE: 'PUSH_READY_TO_USE',
        PUSH_SEARCH_RESUTL: 'PUSH_SEARCH_RESUTL',
        PUSH_CLOSE_CONNECTION: 'PUSH_CLOSE_CONNECTION',
        PUSH_PRODUCT_REQUEST: 'PUSH_PRODUCT_REQUEST',
        SYNC_REQUEST_PUSH: 'SYNC_REQUEST_PUSH',
        SYNC_REQUEST_PUSH_SUCCESS: 'SYNC_REQUEST_PUSH_SUCCESS',
        GO_CLOSE: 'GO_CLOSE',
        TURN_ON_CHAT: 'TURN_ON_CHAT',
        TURN_OFF_CHAT: 'TURN_OFF_CHAT',
        GET_CLONE: 'GET_CLONE',
        CLONE_RESULT: 'CLONE_RESULT',
        EXCHANGE:{
        	RUNNING:'RUNNING'
        },
        GET_SUB_SHOP:'GET_SUB_SHOP',
        SET_SUB_SHOP:'SET_SUB_SHOP',
        SYN_STATE_FOLLOWED: 'SYN_STATE_FOLLOWED',
        CLOSE_CONNECTION_EXCHANGE: 'CLOSE_CONNECTION_EXCHANGE',
        EXCHANGE_ACTIVE: 'EXCHANGE_ACTIVE'
    },
    TEMPLATE: {
        CSS_CHAT:`
            <style id="shopeeGo_chat_style">
                body, body > *{
                    width: 0;
                    overflow-x: hidden !important;
                    overflow-y: hidden !important;
                }
                .shopee-chat-container {
                    top:0;
                    left: 0;
                    position: fixed;
                }
                .main-header {
                    position: initial;
                }
            </style>
        `,
       CSS_LIKE:`
    	    <style>
				body{
				  position: relative;
				}
				.overlay{
				  position: absolute;
				  top: 0;
				  left: 0;
				  width: 100%;
				  height: 100%;
				  z-index: 1001;
				  background-color: rgba(0,0,0,0.5); 
				}
			 </style>
			 <div class="overlay"></div>
	      `
    }
}