const electron = require('electron')
const {app, BrowserWindow} = electron
const con = require('./constant.js')
const fs = require('fs')
//development
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';
const path = require('path')
var myWindow = null
var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
    if (myWindow) {
        if (myWindow.isMinimized()) myWindow.restore()
        myWindow.focus()
    }
})

if (shouldQuit) {
    app.quit()
    return
}


global.index_window = null
global.shopee_window = []

app.on('ready', () => {
    global.index_window = new BrowserWindow({
        width: con.WINDOW.MAIN.width,
        height:con.WINDOW.MAIN.height,
        resizable: false
    })
    global.index_window.on("close", () => {
        app.quit();
    })
    //global.index_window.webContents.openDevTools()
    global.index_window.loadURL(con.ROUTE_INDEX)
    global.index_window.webContents.session.on('will-download', (event, item, webContents) => {
        console.log('-------download--------')
        item.setSavePath(app.getPath('downloads'))
    })
    electron.powerMonitor.on('resume', () => {
        global.index_window.reload()
    })
    
    
})

var deleteFolderRecursive = function(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function(file, index){
            var curPath = path + "/" + file
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath)
            } else { // delete file
                fs.unlinkSync(curPath)
            }
        })
        fs.rmdirSync(path)
    }
}
exports.openWindow = (config, callback) => {
    var opt = {
        width: config.width,
        height: config.height,
        show: config.isShow,
        webPreferences: {
            nodeIntegration: false,
            allowRunningInsecureContent: true,
            allowDisplayingInsecureContent: true,
            webSecurity: false,
            partition: 'persist:' + config.user,
            preload: path.join(__dirname, config.preload)
        }
    }
    if(config.parent == true) {
        opt.parent = global.index_window
        opt.modal = true
    }
    var win = new BrowserWindow(opt)
    //win.webContents.openDevTools()
    win.loadURL(config.filename)
    win.webContents.on('did-finish-load', function() {
        if(config.style) {
            for(var i = 0; i < config.style.length; i++) {
                fs.readFile(config.style[i], "utf-8", function(error, data) {
                    if(!error){
                        var formatedData = data.replace(/\s{2,10}/g, ' ').trim()
                        win.webContents.insertCSS(formatedData)
                    }
                })
            }
        }
        if(config.script) {
            for(var i = 0; i < config.script.length; i++) {
                fs.readFile(config.script[i], "utf-8", function(error, data) {
                    if(!error){
                        win.webContents.executeJavaScript(data)
                    }
                })
            }
        }
    })
    win.webContents.on('new-window', (event, url) => {
        event.preventDefault()
    })
    if(config.preload == con.GO.GO) {
        win.webContents.on('close', (event, url) => {
            global.index_window.webContents.send (con.IPC.GO_CLOSE, {})
        })
    }
    global.shopee_window.push(win)
    if(callback) callback(win)
}
exports.getProcess = (callback) => {
    callback({
        memory: process.getProcessMemoryInfo(),
        cpu: process.getCPUUsage(),
        system: process.getSystemMemoryInfo(),
        all: global.index_window
    })
}
exports.getPathDocument = () => {
    return app.getPath('documents')
}
exports.deleteFolderPrint = () => {
    deleteFolderRecursive(app.getPath('documents')+ '/shopeeGoPrint')
}
exports.deletePartionFolder = (list) => {
    const { lstatSync, readdirSync } = require('fs')
    const { join } = require('path')
    const isDirectory = source => lstatSync(source).isDirectory()
    const getDirectories = source => readdirSync(source).map(name => join(source, name)).filter(isDirectory)
    var allPartions = getDirectories(app.getPath('userData') + '/Partitions')

    var listPath = app.getPath('userData') + '/Partitions/'
    for(var i = 0; i < allPartions.length; i++) {
        var isDelete = true
        for(var j = 0; j < list.length; j++) {
            if(allPartions[i] == listPath + list[j]) {
                isDelete = false
                break
            }
        }
        if(isDelete) {
            deleteFolderRecursive(allPartions[i])
        }
    }
}
exports.getPathDownload = () => {
    return app.getPath('downloads')
}