const { remote , ipcRenderer } = require ('electron')
const con = remote.require('./constant.js')
let index_window = remote.getGlobal ('index_window')
var isActive = true;
    
window.addEventListener('load', ()  => {
	window.$ = window.jQuery = require('jquery')
    require('jquery.cookie')
	window.axios = require('axios')	
    $('body').append(con.TEMPLATE.CSS_LIKE);
	var delay = ms => new Promise(resolve => setTimeout(resolve, ms));
    var totalItem =  0;
    var first =  true;
    var offset = 0;
    var listProduct = [];
    var loopsubcire = null;
    var shopName = window.location.pathname.substr(1);
    function run() {
    	getIds([shopName]).then((res)=>{
        	 let shops = res.data;
    	   	 let shopName = '';
    	   	 let shopId = '';
    	   	 for (var k in shops[0]){
    	   		 shopName = k;
    	   		 shopId = shops[0][k];
    	   	 }
    	   	 if(shops['followed']){
    			 index_window.webContents.send(con.IPC.SYN_STATE_FOLLOWED,{"is_follow":shops['followed'] , 'shopid': shopId});
    		 }else{
    			 console.log('--subcire');
    			 setTimeout(()=>{
    				console.log('subcire');
    				loopsubcire = subcire(shopId);
                 }, 1000*60*5)
    		 }
        	 getInfos([shopId]).then((res)=>{  
        		 shops = res.data[0];
        		 getAllProduct(shopId,0).then((res)=>{ 
        			 let data = res.data;
        			 for ( i = 0; i < data.items.length; i++) {
    	        		if(!data.items[i].liked){
    	        			listProduct.push(data.items[i]);
    	        		}
    				 }
        			 var j = 0 ;
        			 async function loop() {
    	                setTimeout(()=>{
    	                	likeProduct(shopId , listProduct[j].itemid).then((res)=>{ 
    	                		  console.log('like product');
    	        				  j++;
    	        				  if(listProduct.length > j){
    	        					  loop();
    	        				  }
    	        			});
    	                }, 4000)
    	            }
        			if(listProduct.length > 0){
        				loop();
        		    }
        		 });
        	 });
        });
	}
    
	function getIds(usersname) {
	    return axios.post(con.SHOPEE.GET_IDS_BY_USERNAME,{'usernames':usersname},{
            headers: {
            	'Cache-Control': 'no-cache' ,
                'Content-Type': 'application/json; charset=UTF-8',
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'x-api-source': 'pc',
                'x-csrftoken': $.cookie('csrftoken'),
                'x-requested-with':'XMLHttpRequest'
            }
        })
	}
    
	function getInfos(shop_ids) {
	    return axios.post(con.SHOPEE.SHOP_INFO,{'shop_ids':shop_ids},{
            headers: {
            	'Cache-Control': 'no-cache' ,
                'Content-Type': 'application/json; charset=UTF-8',
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'x-api-source': 'pc',
                'x-csrftoken': $.cookie('csrftoken'),
                'x-requested-with':'XMLHttpRequest'
            }
        })
	}
	
	    
    function getAllProduct(shopId , offset) {
    	 return axios.get(con.SHOPEE.SEARCH_ITEM+"&limit=100&match_id="+shopId+"&newest="+offset);
	 }
	    
    function likeProduct(shopId , itemId) {
        return axios.post(con.SHOPEE.LIKE_ITEM+''+shopId+'/item/'+itemId+'/',{},{
            headers: {
            	'Cache-Control': 'no-cache' ,
            	'Content-Type':'multipart/form-data',
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'x-api-source': 'pc',
                'x-csrftoken': $.cookie('csrftoken'),
                'x-requested-with':'XMLHttpRequest'
            }
        })
	}	    
	function subcire(shopId) {
		console.log('Subcire '+ shopId);
	    return axios.post(con.SHOPEE.FLOW_SHOP+''+shopId+'/',{},{
            headers: {
            	'Cache-Control': 'no-cache' ,
                'accept': 'application/json, text/javascript, */*; q=0.01',
                'x-api-source': 'pc',
                'x-csrftoken': $.cookie('csrftoken'),
                'x-requested-with':'XMLHttpRequest'
            }
        })
	}
	
	var length = 6;
	var listA = [
		'#main',
		'.shop-collection-view:eq(0)',
		'.shop-collection-view:eq(1)',
		'.shop-page__all-products-section:eq(0)',
		'.shop-search-result-view__item:eq(0)',
		'.shop-search-result-view__item:eq(5)',
		'.shop-search-result-view__item:eq(15)'];
	function scrollActionPixel(element , time) {
		$('html, body').animate({
	        scrollTop:$(element).length ? $(element).offset().top : 0 
	    }, time);
	}
	function randomInteger(arr) {
		return Math.floor(Math.random()*(arr[1]-arr[0]+1)+arr[0]);
	}
	
	ipcRenderer.on(con.IPC.EXCHANGE_ACTIVE, (event, data) => {
		isActive = data.isActive;
    });
	async function scrollX() {
		let r = randomInteger([0,length-1]);
		let t  = randomInteger([3000 , 7000]);	
		setTimeout(() => {
			if(isActive){
				scrollActionPixel(listA[r] , randomInteger([500 , 1000]));
			}
			scrollX();
		}, t);
    }
	run();
    scrollX();
});