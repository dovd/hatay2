const { remote, ipcRenderer } = require ('electron')
const con = remote.require('./constant.js')
let index_window = remote.getGlobal ('index_window')
window.addEventListener('load', ()  => {
    window.$ = window.jQuery = require('jquery')
    window.axios = require('axios')
    require('jquery.cookie')
    require('bluebird')
    var delay = ms => new Promise(resolve => setTimeout(resolve, ms));
    var shopeeGo_user = {}
    function getSPC_CDS() {
        return '&SPC_CDS=' + $.cookie('SPC_CDS')
    }
    function handleErr(err) {
        switch (err) {
            case con.ERR.SERVER_FAIL:
                var window = remote.getCurrentWindow()
                index_window.webContents.send(con.IPC.SYNC_FAIL,window.webContents.getWebPreferences().partition.replace('persist:',''))
                window.destroy()
                break
            case con.ERR.SYNC_NOT_LOGIN:
                var window = remote.getCurrentWindow()
                index_window.webContents.send(con.IPC.SYNC_NOT_LOGIN,window.webContents.getWebPreferences().partition.replace('persist:',''))
                window.destroy()
                break
        }
    }
    function checkLogin() {
        return new Promise((fullfill, reject) => {
            async function loop() {
                if(!navigator.onLine) return setTimeout(()=>{handleErr(con.ERR.SERVER_FAIL)},15000)
                setTimeout(()=>{
                    console.log('Check login')
                    $.get(con.SHOPEE.CHECK_LOGIN + getSPC_CDS())
                        .done(data => {
                            console.log(data)
                            if(data.id) {
                                shopeeGo_user.shopID = data.id
                                return fullfill(data)
                            } else {

                            }
                        }).fail((err)=>{
                            console.log(err)
                        handleErr(con.ERR.SYNC_NOT_LOGIN)
                    })
                }, 5000)
            }
            loop()
        })
    }
    function getInfo(shop) {
        return new Promise((fullfill, reject) => {
            $.get(con.SHOPEE.GET_INFO + shop.id + '/?SPC_CDS_VER=2' + getSPC_CDS())
            .done(data => {
                data = data.users[0]
                console.log(data)
                var window = remote.getCurrentWindow()
                var partition = window.webContents.getWebPreferences().partition.replace('persist:','')
                var user = {
                    partition: partition,
                    user: {
                        username: data.username,
                        avatar: data.portrait,
                        shopid: data.shopid
                    }
                }
                shopeeGo_user = {
                    username: data.username,
                    avatar: data.portrait,
                    shopid: data.shopid
                }
                return fullfill(user)
            }).fail((data)=>{

            })
        })
    }
    function crawInfo(user) {
        return new Promise((fullfill, reject) => {
            function getOrder(cb) {
                $.get(con.SHOPEE.GET_ORDER + getSPC_CDS())
                .done(data => {
                    console.log(data)
                    cb(data.meta.total)
                }).fail((err)=>{
                    handleErr(con.ERR.SERVER_FAIL)
                })
            }
            async function loop() {
                setTimeout(()=>{
                    console.log('crawInfo')
                    var numMessage = $(con.SHOPEE_DOM.CHAT).html()
                    if(numMessage) {
                        numMessage = parseInt(numMessage)
                    } else {
                        numMessage = 0
                    }
                    user.user.numMessage = numMessage
                    getOrder((numOrder)=> {
                        user.user.numOrder = numOrder
                        user.disconnected = false
                        index_window.webContents.send(con.IPC.SYNC_SUCCESS, user)
                        loop()
                    })

                }, con.WINDOW.USER_SYNC.TIME_WAIT)
            }
            loop()
        })
    }
    function push() {
        ipcRenderer.on (con.IPC.PUSH_PRODUCT_REQUEST, (event, data) => {
            axios.put('https://banhang.shopee.vn/api/v2/products/'+data.id+'/?SPC_CDS_VER=2' + getSPC_CDS(),{product:{product_command:'boost'}},{
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'accept': 'application/json, text/javascript, */*; q=0.01'
                }
            }).then((res)=>{
                index_window.webContents.send(con.IPC.SYNC_REQUEST_PUSH_SUCCESS, {
                    res: res,
                    detech: data
                })
            }).catch(function (err) {
                console.log(err)
            });
        })
    }
    function chat() {
        ipcRenderer.on (con.IPC.TURN_ON_CHAT, (event, data) => {
            if(!$('#shopeeGo_chat_style').length){
                $('body').append(con.TEMPLATE.CSS_CHAT)
            }
            $(con.SHOPEE_DOM.CHAT_OPEN).click()
            $(con.SHOPEE_DOM.CHAT_CLOSE_BTN).click(() => {
                index_window.webContents.send(con.IPC.TURN_OFF_CHAT,{})
            })
        })
        ipcRenderer.on (con.IPC.TURN_OFF_CHAT, (event, data) => {
            $(con.SHOPEE_DOM.CHAT_CLOSE).prop('onclick', null).off('click')
            $(con.SHOPEE_DOM.CHAT_CLOSE).click()
        })
    }
    function clone() {
        ipcRenderer.on (con.IPC.GET_CLONE, (event, data) => {
            $.get(con.SHOPEE.GET_ALL_PRODUCT + getSPC_CDS() + '&search=' + '&limit=' + 99999 + '&offset=' + 0)
                .done(data => {
                    data.goUser = shopeeGo_user.username
                    index_window.webContents.send (con.IPC.CLONE_RESULT, data)
                }).fail((err)=>{

            })
        })
    }
    function run() {
        checkLogin().then(getInfo).then(crawInfo)
        push()
        chat()
        clone()
    }
    run()
});
