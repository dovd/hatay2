const { remote, ipcRenderer } = require ('electron')
const con = remote.require('./constant.js')
const main = remote.require('./main.js')
var fs = require('fs')
var path = require('path')
let index_window = remote.getGlobal ('index_window')
if (index_window) index_window.webContents.send ('message', "Im here")
ipcRenderer.on ('message', (event, message) => { console.log (message) })
function ensureDirectoryExistence(filePath) {
    var dirname = path.dirname(filePath)
    if (fs.existsSync(dirname)) {
        return true;
    }
    ensureDirectoryExistence(dirname)
    fs.mkdirSync(dirname)
}
window.addEventListener('load', ()  => {
    window.$ = window.jQuery = require('jquery')
    var name = new Date().getTime()
    var path = main.getPathDocument() + '/shopeeGoPrint/' + name + '.html'
    try {
        ensureDirectoryExistence(path)
        var str = document.documentElement.outerHTML.split('<html><head>')
        str = '<html><head>' + '<script>window.print()</script>' + str[1]
        fs.writeFileSync(path, str, 'utf-8')
        require('electron').shell.openItem(path)
        remote.getCurrentWindow().destroy()
    }
    catch(e) {
        console.log(e)
    }
});
