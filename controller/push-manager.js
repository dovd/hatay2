const { remote, ipcRenderer } = require ('electron')
const con = remote.require('./constant.js')
let index_window = remote.getGlobal ('index_window')
var win = remote.getCurrentWindow()
var partition = win.webContents.getWebPreferences().partition.replace('persist:','')
ipcRenderer.on (con.IPC.PUSH_CLOSE_CONNECTION, (event, data) => {
    remote.getCurrentWindow().destroy()
})
window.addEventListener('load', ()  => {
    window.$ = window.jQuery = require('jquery')
    require('jquery.cookie')
    require('bluebird')
    var delay = ms => new Promise(resolve => setTimeout(resolve, ms));
    function getSPC_CDS() {
        return '&SPC_CDS=' + $.cookie('SPC_CDS')
    }
    function handleErr(err) {
        switch (err) {
            case con.ERR.SERVER_FAIL:
                index_window.webContents.send(con.IPC.SYNC_FAIL,win.webContents.getWebPreferences().partition.replace('persist:',''))
                win.destroy()
                break
            case con.ERR.SYNC_NOT_LOGIN:
                index_window.webContents.send(con.IPC.SYNC_NOT_LOGIN,win.webContents.getWebPreferences().partition.replace('persist:',''))
                win.destroy()
                break
        }
    }
    function checkLogin() {
        return new Promise((fullfill, reject) => {
            async function loop() {
                if(!navigator.onLine) return setTimeout(()=>{handleErr(con.ERR.SERVER_FAIL)},15000)
                setTimeout(()=>{
                    console.log('Check login')
                    $.get(con.SHOPEE.CHECK_LOGIN + getSPC_CDS())
                        .done(data => {
                            console.log(data)
                            if(data.id) {
                                return fullfill(data)
                            } else {

                            }
                        }).fail((err)=>{
                        console.log(err)
                        handleErr(con.ERR.SYNC_NOT_LOGIN)
                    })
                }, 5000)
            }
            loop()
        })
    }
    function readyToUse(data) {
        index_window.webContents.send (con.IPC.PUSH_READY_TO_USE, {
            username: data.username
        })
        ipcRenderer.on (con.IPC.CALL_GET_PRODUCT, (event, data) => {
            $.get(con.SHOPEE.SEARCH_PRODUCT + getSPC_CDS() + '&search=' + encodeURI(data.text ? data.text.trim():'') + '&limit=' + data.limit + '&offset=' + data.skip)
                .done(data => {
                    window.x = data
                    index_window.webContents.send (con.IPC.PUSH_SEARCH_RESUTL, data)
                }).fail((err)=>{

            })
        })
    }
    function run() {
        checkLogin().then(readyToUse)
    }
    run()
});
// axios.put('https://banhang.shopee.vn/api/v2/products/'+x.products[8].id+'/?SPC_CDS=dc329450-8a5b-4087-91c6-448590b5c7fa&SPC_CDS_VER=2',{product:{product_command:'boost'}},{
//     headers: {
//         'Content-Type': 'application/json; charset=UTF-8',
//         'accept': 'application/json, text/javascript, */*; q=0.01'
//     }
// }).then((res)=>{
//     console.log(res)
// })