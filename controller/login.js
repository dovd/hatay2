const { remote, ipcRenderer } = require ('electron')
const con = remote.require('./constant.js')
let index_window = remote.getGlobal ('index_window')
if (index_window) index_window.webContents.send ('message', "Im here")
ipcRenderer.on ('message', (event, message) => { console.log (message) })
window.addEventListener('load', ()  => {
    window.$ = window.jQuery = require('jquery')
    require('jquery.cookie')
    require('bluebird')
    var delay = ms => new Promise(resolve => setTimeout(resolve, ms));
    function getSPC_CDS() {
        return '&SPC_CDS=' + $.cookie('SPC_CDS')
    }
    function checkLogin() {
        return new Promise((fullfill, reject) => {
            async function loop() {
                setTimeout(()=>{
                    console.log('Check login')
                    $.get(con.SHOPEE.CHECK_LOGIN + getSPC_CDS())
                    .done(data => {
                        console.log(data)
                        if(data.id) {
                            return fullfill(data)
                        } else {

                        }
                    }).fail((err)=>{
                        console.log('Not login')
                        loop()
                    })
                }, 5000)
            }
            loop()
        })
    }
    function getInfo(shop) {
        return new Promise((fullfill, reject) => {
            $.get(con.SHOPEE.GET_INFO + shop.id + '/?SPC_CDS_VER=2' + getSPC_CDS())
            .done(data => {
                data = data.users[0]
                console.log(data)
                var window = remote.getCurrentWindow()
                var partition = window.webContents.getWebPreferences().partition.replace('persist:','')
                index_window.webContents.send (con.IPC.LOGIN_SUCCESS, {
                    partition: partition,
                    user: {
                        username: data.username,
                        avatar: data.portrait,
                        numOrder: 0,
                        numMessage: 0,
                        shopId: data.shopid
                    },
                    notLogin: false
                })
                window.destroy()
            }).fail((err)=>{
            		
            })
        })
    }
    function run() {
        checkLogin().then(getInfo)
    }
    run()
});

//window.onbeforeunload = (e) => {
//    var ans = confirm(con.NOTI.ALERT_CLOSE_LOGIN_WINDOW)
//    if(ans) {
//        index_window.webContents.send (con.IPC.LOGIN_CLOSE, {})
//    } else {
//        e.returnValue = false
//    }
//}