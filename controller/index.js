const { remote, ipcRenderer } = require ('electron')
const main = remote.require('./main.js')
const SecureLS = require('secure-ls')
const moment = require('moment')
var Promise = require('bluebird')
var later = require('later')
var XLSX = require('xlsx')
var axios =require('axios')
later.date.localTime()
let ls = new SecureLS()
let con = remote.require('./constant.js')
let service = axios.create({
  baseURL: con.API.BASE, // api base_url
  timeout: 5000 // request timeout
})
service.interceptors.response.use(
  response => {
	  if(typeof response.headers.authorization !== 'undefined'){
		  const authorization = response.headers.authorization;
		  let newtoken = authorization.replace('Bearer ' , '');
	      ls.set(con.LS.TOKEN , newtoken);
	  }
	  return response;
  },
  error => {
	  if (!error.status && error.response == undefined) {
		  utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.CONNECT_AFTER})
	  }else{
		  let resData = error.response.data;
		  	let undefinedMsgs = '';
		  	for (var key in resData) {
		  	  if(resData[key].length > 0){
					  undefinedMsgs += resData[key][0];
					  undefinedMsgs += '<br />';
		  	  }
		  	}
		  	if(undefinedMsgs !== ''){
		  		utils.alert({title: con.NOTI.DEFAULT_ERR, msg: undefinedMsgs})
		  	}
		  	if(error.response.status !== 422){
		  		localStorage.removeItem(con.LS.USERNAME);
		  		localStorage.removeItem(con.LS.PASSWORD);
		  	}
	  }
	  return Promise.reject(error)
  }
)
service.interceptors.request.use(
  config => {
    if (ls.get(con.LS.TOKEN)) {
    	config.headers['Authorization'] = 'Bearer ' + ls.get(con.LS.TOKEN);
    }
    config.headers['X-localization'] ='vi';
    return config
  },
  error => {
    Promise.reject(error)
  }
)


Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
    }
})
Vue.filter('vnd', function(value) {
    if (value) {
        function funkyStringSplit( s ){
            var i = s.length % 3;
            var parts = i ? [ s.substr( 0, i ) ] : [];
            for( ; i < s.length ; i += 3 )
            {
                parts.push( s.substr( i, 3 ) );
            }
            return parts;
        }
        var str = value + '';
        arr = funkyStringSplit(str);
        if(arr.length == 0 || value==undefined) return '';
        if(arr.length ==1) return str + ' đ';
        if(arr.length > 1){
            var x = arr[0];
            for(var i = 1; i < arr.length; i++){
                x += ',' + arr[i];
            }
            x += 'đ';
            return x;
        }
    }
})
Vue.filter('gb', function(value) {
    if (value) {
        return (parseInt(value)/(1024*1024)).toFixed(2) + ' GB'
    }
})
Vue.filter('limittext', function(value) {
    if (value) {
        return value.substring(0, 25) + '...'
    }
})
var app = new Vue({
    el: '#app',
    data: {
        version: 1,
        server_version: 0,
        low_version: false,
        config: {
            bannerImg: '',
            bannerLink: '',
            percore: 0
        },
        sample: {
            avatarShop:'../assets/shop-icon.jpg',
            isPush: true,
            overTime: function () {
                console.log('Push!!now')
            },
        },
        app: {
            in: false,
            _in: false,
            begin: 0,
            loading: false,
            loading_msg: '',
            backFade_chat: false,
            exchange: {
            	shopid:'',
            	status: con.STATUS.STOP,
            	point: 0,
            	next_time: 0,
            	status_name: con.STATUS.STOP_NAME
            },
            time_follow:60000*6,
            exchanges:ls.get(con.LS.EXCHANGES) ? ls.get(con.LS.EXCHANGES) : [],
            list: ls.get(con.LS.LIST) ? JSON.parse(ls.get(con.LS.LIST)) : [],
            go: null,
            clone: {
                list:[],
                itemModels: [],
                search: '',
                tiento:'',
                hauto:'',
                giachenhlech:'',
                isCheckAll: false,
                username:''
            }
        },
        browserSync: [],
        view: {
            username:'',
            password:'',
            me: {},
            is_loading: false
        },    
        register: {
        	username:'',
            password:'',
            shopeelink: '',
            is_loading: false
        },
        pushP: {
            searchIndex: null,
            searchStr: '',
            searchList: [],
            searchOn: false,
            searchWin: null,
            searchLoading: true,
            searchLimit: 10,
            searchCurrentPage: 0,
            searchTotal: 0,
            selectedList: [],
            pushHour: 0,
            pushMinute: 0
        },
        process: {system:{total:0}},
        maxSystem: 0
    },
    computed: {
        totalMessageUnRead: function () {
            var count = 0
            for(var i = 0; i < this.app.list.length; i++) {
                count += this.app.list[i].user.numMessage
            }

            $('#totalMessageUnRead').tooltip('update')
            return count
        },
        totalOrderUnRead: function () {
            var count = 0
            for(var i = 0; i < this.app.list.length; i++) {
                count += this.app.list[i].user.numOrder
            }
            $('#totalOrderUnRead').tooltip('update')
            return count
        },
        filteredClone() {
            return this.app.clone.list.filter(item => {
                return item.name.toLowerCase().indexOf(this.app.clone.search.toLowerCase()) > -1
            })
        }
    },
    watch: {
        'pushP.searchStr': function (newStr, oldStr) {
            if(!this.pushP.searchOn) return
            utils.searchDelay(1000, () => {
                this.pushP.searchCurrentPage = 0
                this.callSearchPush()
            })
        }
    },
    created: function() {
        if(ls.get(con.LS.LAST_USE)){
            var last_use = moment(ls.get(con.LS.LAST_USE))
            if(moment().isBefore(last_use)) {
                this.clickLogout()
            }
        }
        this.setLoading(true, con.NOTI.SLOGAN)
        $.get(con.API.CONFIG).done((data) => {
        	//console.log(data);
        	//data = JSON.parse(data);
            this.config.bannerImg = data.image
            this.config.bannerLink = data.link
            this.config.percore = data.percore
            ls.set(con.LS.CONFIG, JSON.stringify(this.config))
            if(data.version > this.version) {
                this.low_version = true
                this.server_version = data.version
                this.handleErr(con.ERR.LOW_VERSION)
            } else {
                this.setLoading(false, '')
                this.checkLogin()
            }
            this.setLoading(false, '')
        }).fail(()=>{
            console.log(JSON.parse(ls.get(con.LS.CONFIG)))
            this.config = JSON.parse(ls.get(con.LS.CONFIG))
            this.setLoading(false, '')
            this.checkLogin()
        })
    },
    methods: {
        processSync: function() {
            main.getProcess((data) => {
                this.process = data
                this.maxSystem = Math.ceil(this.process.system.total * this.config.percore / 1024/1024)
            })
        },
        checkLogin: function() {
            this.view.username = ls.get(con.LS.USERNAME)
            this.view.password = ls.get(con.LS.PASSWORD)
            var pr = this;
            const data = {'email': pr.view.username, 'password': pr.view.password}
            var outdate = moment(parseInt(ls.get(con.LS.OUTDATE)))
            if(!this.view.username && ! this.view.password) {
                this.userLogin(false)
                this.removeLS([con.LS.USERNAME, con.LS.PASSWORD])
            }else if(outdate.isAfter(moment()) && moment().isBefore(moment(ls.get(con.LS.OUTDATE_SHOPEELIKE))) ){
//                /*Init feature*/
                this.listenLogin()
                this.exchageTick();
                this.listenSync()
                this.listenPush()
                this.listenGo()
                this.syncInfo()
                /*END*/
                this.view.me = JSON.parse(ls.get(con.LS.VIEW_USER))
                this.config = JSON.parse(ls.get(con.LS.CONFIG))
                this.processSync()
                this.userLogin(true)
                service({
                    url: '/loginClient',
                    method: 'post',
                    data
                  }).then(function (response) {
                	   pr.initExchanges(response.data.data.balances);
                  }).catch(function (error) {
                	  this.userLogin(false)
                      this.removeLS([con.LS.USERNAME, con.LS.PASSWORD])
                  });
            	//this.clickLogin();
            } else {
            	this.clickLogin();
            }
        },
        clickRegister: function() {
         	const pr = this;
         	const data = {'email': pr.register.username, 'password': pr.register.password};
         	pr.register.is_loading = true;
         	service({
                url: '/registerClient',
                method: 'post',
                data
            }).then(function (response) {
        	   var $modal = $('#modal-register');
        	   pr.register.username = '';
        	   pr.register.password = '';
        	   utils.alert({
        		   title: con.NOTI.REGISTER_TITLE_SUCCESS,
        		   msg: con.NOTI.REGISTER_CONTENT_SUCCESS,
        		   callback: function () {
        			   $modal.modal('hide');
        		   }
        	   });
        	   pr.register.is_loading = false;
           }).catch(function (error) {
        	   pr.register.is_loading = false;
           });
		},
        clickLogin: function() {
        	const pr = this;
        	const data = {'email': pr.view.username, 'password': pr.view.password}
        	pr.view.is_loading = true;
            service({
                url: '/loginClient',
                method: 'post',
                data
              }).then(function (response) {
            	  var now = new Date();
            	  var me = response.data.data;
            	  var outdate = new Date(now.getFullYear(), now.getMonth(),now.getDate(), 23, 59, 59, 0).getTime();
            	  ls.set(con.LS.USERNAME, me.email)
            	  ls.set(con.LS.PASSWORD, pr.view.password)
            	  ls.set(con.LS.VIEW_USER, JSON.stringify(me))
            	  ls.set(con.LS.CONFIG, JSON.stringify(this.config))
            	  ls.set(con.LS.TOKEN, response.data.token)
            	  ls.set(con.LS.OUTDATE, outdate)
            	  ls.set(con.LS.OUTDATE_SHOPEELIKE, me.expiry_date)
            	   pr.initExchanges(me.balances);
            	   pr.view.me = me
            	   /* Init feature */
            	   pr.listenLogin()
            	   pr.listenSync()
            	   pr.listenPush()
            	   pr.listenGo()
            	   pr.syncInfo()
            	   /* END */
            	   pr.processSync()
            	   pr.userLogin(true)
            	   pr.view.is_loading = false;
            	   //pr.setLoading(true, con.NOTI.LOADING_SERVER)
            	   location.reload();
            	   //pr.setLoading(false, con.NOTI.LOADING_SERVER)
              }).catch(function (error) {
           	   	 pr.view.is_loading = false;
           	   	 pr.setLoading(false, con.NOTI.LOADING_SERVER)
              });
        },
        clickLogout: function() {
            this.removeLS([con.LS.USERNAME, con.LS.PASSWORD])
            location.reload()
        },
        clickOpenShopeeLike: function() {
            require('electron').shell.openExternal(con.ROUTE_OPEN_LANDING_PAGE)
        },
        clickOpenQC: function(){
            if(this.config.bannerLink) require('electron').shell.openExternal(this.config.bannerLink)
        },
        handleErr: function(err) {
            console.log(err)
            switch (err) {
                case con.ERR.SERVER_FAIL:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.LOGIN_FAIL})
                    break
                case con.ERR.PASSWORD_REQUIRE:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.USERNAME_PASSWORD_REQUIRE})
                    break
                case con.ERR.USERNAME_REQUIRE:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.USERNAME_PASSWORD_REQUIRE})
                    break
                case con.ERR.NOT_FOUND_USER:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.NOT_FOUND_USER})
                    break
                case con.ERR.NOTTDC:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.NOTTDC})
                    break
                case con.ERR.NEW_USER_WRONG_FORMAT:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.NEW_USER_WRONG_FORMAT})
                    break
                case con.ERR.NEW_USER_EXIST:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.NEW_USER_EXIST})
                    break
                case con.ERR.MAX_PUSH:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.MAX_PUSH})
                    break
                case con.ERR.PUSH_EXIST:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.PUSH_EXIST})
                    break
                case con.ERR.PUSH_TIMER_MINUTE_IN_CORRECT:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.PUSH_TIMER_MINUTE_IN_CORRECT})
                    break
                case con.ERR.PUSH_TIMER_HOUR_IN_CORRECT:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.PUSH_TIMER_HOUR_IN_CORRECT})
                    break
                case con.ERR.PUSH_TIMER_LIST_IN_CORRECT:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.PUSH_TIMER_LIST_IN_CORRECT})
                    break
                case con.ERR.PUSH_TIMER_LIST_MAX:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.PUSH_TIMER_LIST_MAX})
                    break
                case con.ERR.MAX_USER_MANAGER:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.MAX_USER_MANAGER})
                    break
                case con.ERR.LOW_VERSION:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.LOW_VERSION})
                    break
                case con.ERR.DOWNLOAD_EXCEL_EMPTY:
                    utils.alert({title: con.NOTI.DEFAULT_ERR, msg: con.NOTI.DOWNLOAD_EXCEL_EMPTY})
                    break
            }
        },

        userLogin: function(isLogin) {
            this.app.in = isLogin
            this.app._in = isLogin
        },

        removeLS: function(arr) {
          for(var i = 0; i < arr.length; i++) {
              localStorage.removeItem(arr[i])
          }
        },
    	clearLoopExchange(exchange_){
    		if(exchange_.currentLoop != null){
    			clearTimeout(exchange_.currentLoop);
    		}
    		if(exchange_.currentFollow != null){
    			clearTimeout(exchange_.currentFollow);
    		}
    	},
        initExchanges(balances){
        	let seft = this;
        	seft.app.exchanges = [];
        	ls.set(con.LS.EXCHANGES, seft.app.exchanges);
        	var exchange_ = {
                	shopid: 0,
                	status: con.STATUS.STOP,
                	point: 0,
                	status_name: con.STATUS.STOP_NAME,
                	next_time: 0,
                	tick: null,
                	currentLoop: null,
                	currentFollow: null,
                	currentPoint: null,
                	currentShopId: null,
                	isActive: true,
                	winEX: null,
                	partition: null,
                	user: null
            }
        	for (var i = 0; i < balances.length; i++) {
        		exchange_.shopid = balances[i].shop_id;
        		exchange_.point = balances[i].balance_point;
        		var e = Object.assign({}, exchange_);
        		seft.app.exchanges.push(e);
			}
        	ls.set(con.LS.EXCHANGES, seft.app.exchanges);
        },
        getExchange(user){
        	let seft = this;
        	var exchange_ = {
                	shopid: user.shopid,
                	status: con.STATUS.STOP,
                	point: 0,
                	status_name: con.STATUS.STOP_NAME,
                	next_time: 0
            }
        	for (var i = 0; i < seft.app.exchanges.length; i++) {
        		let currentExchange = seft.app.exchanges[i];
				if(currentExchange.shopid == user.shopid){
					return currentExchange;
				}
			}
        	return exchange_;
        },
        clickStopExchangePoint(user){
        	let seft = this;
        	let exchange_;
        	for (var i = 0; i < seft.app.exchanges.length; i++) {
        		let currentExchange = seft.app.exchanges[i];
				if(currentExchange.shopid == user.shopid){
					exchange_ = currentExchange;
				}
			}
        	exchange_.status = 2;
        	exchange_.status_name = con.STATUS.STOPING_NAME;
        	utils.confirm({
		   		title: con.NOTI.STOP_EXCHANGE_TITLE,
		   		msg: con.NOTI.STOP_EXCHANGE_CONTENT,
		   		callback: function() {
		   			exchange_.next_time = 0;
		   			location.reload();
				},
				callbackCancel: function() {
					 exchange_.status = 1;
			         exchange_.status_name = con.STATUS.RUNNING_NAME;
				}
		   });
        },
        exchageTick(){
        	var seft = this;
        	var TIME_FOLLOW = this.app.time_follow;
        	var data_followed = null;
        	ipcRenderer.on (con.IPC.SYN_STATE_FOLLOWED, (event, data) => {
        		data_followed = data;
        	});
        	setInterval(() => {
        		console.log('tick');
        		for (var i = 0; i < seft.app.exchanges.length; i++) {
            		let exchange_ = seft.app.exchanges[i];
            		if(data_followed != null && data_followed.is_follow && exchange_.currentShopId == data_followed.shopid){
            			seft.clearLoopExchange(exchange_);
            	 		seft.serviceShopFollow(exchange_.user.shopid,exchange_.currentShopId,false);
            	 		exchange_.next_time = 1;
            	 		data_followed = null;
            		}
    				if(exchange_.status == con.STATUS.RUNNING || exchange_.status == con.STATUS.WAIT){
    					if(exchange_.next_time == (TIME_FOLLOW/1000)){
			        	   seft.serviceGetSubShop(exchange_.shopid , function(data) {
			        		   exchange_.currentShopId = data.id;
			        		   if(data.id == -1){
			        			   if(exchange_.winEX != null){
			        				   exchange_.winEX.hide();
			              		   };
			              		   exchange_.status = con.STATUS.WAIT;
			              		   exchange_.status_name = 'Chưa tìm thấy shop';
			              	   }else{
			              		   exchange_.status = con.STATUS.RUNNING;
			              		   exchange_.status_name = 'Đang tương tác "'+ data.name+'"';
			              		   let fileName = con.SHOPEE.HOME +data.name;
			              		   if(exchange_.winEX == null){
			              			 var opt = {
			                                 filename: fileName,
			                                 isShow: true,
			                                 user: exchange_.partition,
			                                 width: 666,
			                                 height: 666,
			                                 preload: con.GO.LIKE
			                             }
			                    	 	main.openWindow(opt, win => {
			                    	 		exchange_.winEX = win;
			                    	 		exchange_.currentFollow = seft.waitRunShopFollow(data , exchange_ ,true);
				                    	    exchange_.winEX.on('closed', function() {
				                    	    	exchange_.winEX = null;
				                    	    	seft.clearLoopExchange(exchange_);
				                    	 		location.reload();
				                    	 	});
				                    	    exchange_.winEX.on('blur', function() {
				                    	    	exchange_.isActive = false;
				                    	 		exchange_.winEX.webContents.send(con.IPC.EXCHANGE_ACTIVE, {'isActive':exchange_.isActive})
				                    	 	});
				                    	    exchange_.winEX.on('focus', function() {
				                    	 		isActive = true;
				                    	 		exchange_.winEX.webContents.send(con.IPC.EXCHANGE_ACTIVE, {'isActive':exchange_.isActive})
				                    	 	});
				                         });
			              		   }else{
			              			   if(exchange_.winEX != null){
			              				  exchange_.winEX.show();
			              				  exchange_.currentFollow = seft.waitRunShopFollow(data,exchange_,true);
			              				  exchange_.winEX.loadURL(fileName);
			              				  exchange_.winEX.webContents.send(con.IPC.EXCHANGE_ACTIVE, {'isActive':exchange_.isActive})
			              			   }
			              		   }
			              	   }
						    });
    						exchange_.next_time -= 1;
    					}else if(exchange_.next_time == 0){
    	        			exchange_.next_time = TIME_FOLLOW/1000;
    	        		}else{
    	        			exchange_.next_time -= 1;
    	        		}
    				}
    			}
			}, 1000);
        },
        clickExchangePoint: function(partition , user) {
        	var seft = this;
            var TIME_FOLLOW = this.app.time_follow;
            var is_add = false;
            var runing_count = 1;
        	for (var i = 0; i < seft.app.exchanges.length; i++) {
        		let currentExchange = seft.app.exchanges[i];
				if(currentExchange.status == con.STATUS.RUNNING){
					runing_count++;
				}
			}
        	if(runing_count > 2 && seft.view.me.is_admin == '0'){
        		utils.alert({
                    title: con.NOTI.CONFIRM,
                    msg: con.NOTI.SET_DEFAULT_ID
                });
        		return;
        	}
            for (var i = 0; i < seft.view.me.balances.length; i++) {
				if(seft.view.me.balances[i].shop_id == user.shopid){
					is_add = true;
					break;
				}
			}
        	if(!is_add){
            	const data = {'shopid': user.shopid , 'shopname':user.username,'is_default': false   };
            	service({
  	                url: 'shop/default',
  	                method: 'post',
  	                data
  	           }).then(function (response) {
  	             var me = response.data.data.user;
  	             seft.initExchanges(me.balances);
  	             seft.view.me = me
  	             ls.set(con.LS.VIEW_USER, JSON.stringify(seft.view.me))
    	           exchange_ = seft.getExchange(user);
    	           exchange_.status = con.STATUS.RUNNING;
    	           exchange_.status_name = con.STATUS.RUNNING_NAME;
    	           exchange_.partition = partition;
    	           exchange_.user = user;
  	           });
        	}else{
                exchange_ = seft.getExchange(user);
                exchange_.status = con.STATUS.RUNNING;
                exchange_.status_name = con.STATUS.RUNNING_NAME;
                exchange_.partition = partition;
                exchange_.user = user;
        	}
		},
		clickSetDefaultShop(partition , user){
		  const data = {'shopid': user.shopid , 'shopname':user.username, 'is_default': true  };
		  var seft = this;
		  utils.confirm({
        title: con.NOTI.CONFIRM,
        msg: con.NOTI.SET_DEFAULT_ID_CONTENT,
        okText: con.NOTI.YES,
        cancelText:con.NOTI.NO,
        callback:() => {
          service({
            url: '/shop/default',
            method: 'post',
            data
          }).then(function (response) {
            var me = response.data.data.user;
            seft.initExchanges(me.balances);
            seft.view.me = me
            ls.set(con.LS.VIEW_USER, JSON.stringify(seft.view.me))
          });
        }
		  })
		},
		waitRunShopFollow: function(data,exchange_ , is_point) {
			var seft = this;
			return setTimeout(() => {
				ls.set(con.LS.POINT , ++exchange_.point);
				seft.view.me.point++;
  				seft.serviceShopFollow(exchange_.user.shopid ,data.id , is_point);
			 }, (seft.app.time_follow - 60000));
		},
		serviceShopFollow: function(shopFrom, shopTo,is_point) {
			const data = {'is_point':is_point};
            service({
                url: '/shop/'+shopFrom+'/follow/' + shopTo,
                method: 'post',
                data
	        }).then(function (response) {
	        	  console.log(response.data.data);
	        });
		},
		serviceGetSubShop: function(shopId , cb) {
            service({
                url: '/shop/'+shopId+'/sub',
                method: 'get'
	          }).then(function (response) {
	        	  cb(response.data.data);
	        });
		},
        clickAddAcc: function() {
            if(this.maxSystem <= this.app.list.length) {
                return this.handleErr(con.ERR.MAX_USER_MANAGER)
            }
            var opt = {
                filename: con.SHOPEE.KENH_NGUOI_BAN,
                isShow: true,
                user: utils.generateUUID(),
                width: con.WINDOW.USER_NOMAL.width,
                height: con.WINDOW.USER_NOMAL.height,
                preload: con.GO.LOGIN
            }
            //this.setLoading(true, con.NOTI.LOADING_WAIT_CREATE_USER)
            main.openWindow(opt)
        },
        clickReLoginAcc: function(partiton) {
            var opt = {
                filename: con.SHOPEE.KENH_NGUOI_BAN,
                isShow: true,
                user: partiton,
                width: con.WINDOW.USER_NOMAL.width,
                height: con.WINDOW.USER_NOMAL.height,
                preload: con.GO.LOGIN
            }
            this.setLoading(true, con.NOTI.LOADING_WAIT_CREATE_USER)
            main.openWindow(opt)
        },
        clickDeteAcc: function(index) {
            utils.confirm({
                title: con.NOTI.CONFIRM,
                msg: con.NOTI.DELETE_USER_CONFIRM_TEXT,
                okText: con.NOTI.YES,
                cancelText:con.NOTI.NO,
                callback:() => {
                    this.app.list.splice (index, 1);
                    ls.set(con.LS.LIST, JSON.stringify(this.app.list))
                    location.reload()
                }
            })
        },
        clickPushManager: function(index){
            var opt = {
                filename: con.SHOPEE.KENH_NGUOI_BAN,
                isShow: false,
                user: this.app.list[index].partition,
                width: con.WINDOW.USER_NOMAL.width,
                height: con.WINDOW.USER_NOMAL.height,
                preload: con.GO.PUSH_MANAGER
            }
            this.pushP.searchIndex = index
            Object.assign(this.pushP.selectedList, this.app.list[index].pushArr ? this.app.list[index].pushArr : [])
            this.toggleSearchPush(true, opt)
        },
        toggleSearchPush: function(status, opt) {
            var $pushModal = $('#modal-push')
            if(status) {
                $pushModal.modal('show')
                this.pushP.searchOn = true
                this.pushP.searchLoading = true
                main.openWindow(opt, win => {
                    this.pushP.searchWin = win
                })
            } else {
                console.log(this.app.list)
                this.pushP.searchWin.webContents.send (con.IPC.PUSH_CLOSE_CONNECTION, {})
                this.pushP.searchIndex= null
                this.pushP.searchStr= ''
                this.pushP.searchList= []
                this.pushP.searchOn= false
                this.pushP.searchWin= null
                this.pushP.searchLoading= true
                this.pushP.searchLimit= 10
                this.pushP.searchCurrentPage= 0
                this.pushP.searchTotal= 0
                this.pushP.selectedList= []
                $pushModal.modal('hide')
            }
        },
        clickSavePushList: function() {
            this.app.list[this.pushP.searchIndex].pushArr = this.pushP.selectedList
            ls.set(con.LS.LIST, JSON.stringify(this.app.list))
            console.log(this.app.list)
            this.toggleSearchPush(false)
            location.reload()
        },
        callSearchPush: function() {
            this.pushP.searchLoading = true
            this.pushP.searchWin.webContents.send (con.IPC.CALL_GET_PRODUCT, {
                text: this.pushP.searchStr,
                limit: this.pushP.searchLimit,
                skip: this.pushP.searchLimit * this.pushP.searchCurrentPage
            })
        },
        pushPrevSearch: function() {
            this.pushP.searchCurrentPage --
            this.callSearchPush()
        },
        pushNextSearch: function() {
            this.pushP.searchCurrentPage ++
            this.callSearchPush()
        },
        setLoading: function(stt, message) {
            this.app.loading = stt
            this.app.loading_msg = message
        },
        clickChoicePush: function(product) {
            console.log(product.id)
            if(this.pushP.selectedList.length >= con.MAX_PRODUCT_PUSH) return this.handleErr(con.ERR.MAX_PUSH)
            for(var i = 0; i < this.pushP.selectedList.length; i++) {
                if(this.pushP.selectedList[i].id == product.id) {
                    return this.handleErr(con.ERR.PUSH_EXIST)
                }
            }
            this.pushP.selectedList.push({
                id: product.id,
                name: product.name,
                image: product.images[0],
                mode:0,
                nextTime: new Date(),
                timer: []
            })
        },
        clickPushDeleteSelected: function(index) {
            this.pushP.selectedList.splice(index, 1)
        },
        clickPushChangeTimerMode: function(index){
            if(this.pushP.selectedList[index].mode == 1){
                this.pushP.selectedList[index].mode = 0
            } else {
                this.pushP.selectedList[index].mode = 1
            }
        },
        clickPushAddTimer: function(index) {
            this.pushP.pushHour = parseInt(this.pushP.pushHour )
            this.pushP.pushMinute = parseInt(this.pushP.pushMinute )
            if(this.pushP.pushHour < 0 || this.pushP.pushHour > 23 || typeof this.pushP.pushHour != 'number')
                return this.handleErr(con.ERR.PUSH_TIMER_HOUR_IN_CORRECT)
            if(this.pushP.pushMinute < 0 || this.pushP.pushMinute > 59 || typeof this.pushP.pushMinute != 'number')
                return this.handleErr(con.ERR.PUSH_TIMER_MINUTE_IN_CORRECT)
            var newH = parseInt(this.pushP.pushHour)
            var newM = parseInt(this.pushP.pushMinute)
            if(this.pushP.selectedList[index].timer.length >= 6){
                return this.handleErr(con.ERR.PUSH_TIMER_LIST_MAX)
            }
            function checkTime(h, newH, m, newM) {
                if((h > newH && h - newH < 4) || (newH > h && newH - h < 4) || (h - newH == 0)) {
                    return false
                } else if (newH - h == 4) {
                    if(newM - m < 0) {
                        return false
                    }
                } else if (h - newH == 4) {
                    if(m - newM < 0){
                        return false
                    }
                }
                return true
            }
            for(var i = 0; i < this.pushP.selectedList[index].timer.length; i++) {
                var h = this.pushP.selectedList[index].timer[i].split(':')[0]
                var m = this.pushP.selectedList[index].timer[i].split(':')[1]
                if(!checkTime(h, newH, m, newM)) return this.handleErr(con.ERR.PUSH_TIMER_LIST_IN_CORRECT)
                if(newH + 4 > 23) {
                    var h = newH + 4
                    if(!checkTime(h, newH, m, newM)) return this.handleErr(con.ERR.PUSH_TIMER_LIST_IN_CORRECT)
                }

            }
            this.pushP.selectedList[index].timer.push(newH + ":" + newM)
            this.pushP.pushHour  += 4
            if(this.pushP.pushHour > 23) this.pushP.pushHour = this.pushP.pushHour - 24
        },
        clickPushDeleteTimer: function(index) {
            this.pushP.selectedList[index].timer.pop()
        },
        clickGo: function(index, target){
            remote.getGlobal ('index_window').hide()
            var opt = {
                filename: con.SHOPEE.KENH_NGUOI_BAN,
                isShow: true,
                user: this.app.list[index].partition,
                width: con.WINDOW.USER_NOMAL.width,
                height: con.WINDOW.USER_NOMAL.height,
                preload: con.GO.GO
            }
            if(target == 'order') opt.filename = con.SHOPEE.KENH_NGUOI_BAN_ORDER
            main.openWindow(opt, (win)=>{
                this.app.go = win
            })
        },
        clickChat: function(username) {
            var showChat = (indexOfWin) => {
                console.log(this.browserSync[indexOfWin].win.webContents)
                this.browserSync[indexOfWin].win.show()
                this.browserSync[indexOfWin].win.webContents.send(con.IPC.TURN_ON_CHAT, {})
                ipcRenderer.on (con.IPC.TURN_OFF_CHAT, (event, data) => {
                    hideChat(indexOfWin, false)
                })
                // $('body').click(() => {
                //     if(this.app.backFade_chat) {
                //         hideChat(indexOfWin, true)
                //     }
                //     this.app.backFade_chat = true
                // })
            }
            var hideChat = (indexOfWin, isSendIPC) => {
                if(isSendIPC) {
                    this.browserSync[indexOfWin].win.webContents.send(con.IPC.TURN_OFF_CHAT, {})
                }
                //this.app.backFade_chat = false
                //$('body').prop('onclick', null).off('click')
                this.browserSync[indexOfWin].win.hide()
            }
            for(var i = 0; i < this.browserSync.length; i++) {
                if(this.browserSync[i].username == username) {
                    showChat(i)
                    break
                }
            }
        },
        listenLogin: function(){
            ipcRenderer.on (con.IPC.LOGIN_SUCCESS, (event, data) => {
                this.setLoading(false, '')
                console.log(data)
                for(var i = 0; i < this.app.list.length; i++) {
                    if(this.app.list[i].user.username == data.user.username && this.app.list[i].partition == data.partition) {
                        this.app.list[i].notLogin = false
                        ls.set(con.LS.LIST, JSON.stringify(this.app.list))
                        return location.reload()
                    }
                    if(this.app.list[i].user.username != data.user.username && this.app.list[i].partition == data.partition) {
                        this.app.list[i].partition = utils.generateUUID()
                        ls.set(con.LS.LIST, JSON.stringify(this.app.list))
                        return location.reload()
                    }

                    if(this.app.list[i].user.username == data.user.username) {
                        for(var j = 0; j < this.app.list.length; j++) {
                            if(this.app.list[j].partition == data.partition) {
                                this.app.list[j].partition = utils.generateUUID()
                                ls.set(con.LS.LIST, JSON.stringify(this.app.list))
                            }
                        }
                        return this.handleErr(con.ERR.NEW_USER_EXIST)
                    }
                }
                this.app.list.push(data)
                ls.set(con.LS.LIST, JSON.stringify(this.app.list))
                location.reload()
            })
            ipcRenderer.on (con.IPC.LOGIN_CLOSE, (event, data) => {
                this.setLoading(false, '')
            })
        },
        schedulePush: function(){
            var S = []
            for(var i = 0; i < this.app.list.length; i++) {
                var pushlist = this.app.list[i].pushArr
                console.log(i)
                if(!pushlist) continue
                for(var j = 0; j < pushlist.length; j++){
                    if(pushlist[j].mode == 0) {
                        S.push({
                            mode: pushlist[j].mode,
                            username: this.app.list[i].user.username,
                            id: pushlist[j].id,
                            nextTime: pushlist[j].nextTime
                        })
                    } else {
                        var timelist = pushlist[j].timer
                        var schedules = []
                        for(var k = 0; k < timelist.length; k++) {
                            var hour = parseInt(timelist[k].split(':')[0])
                            var minute = parseInt(timelist[k].split(':')[1])
                            schedules.push({
                                h:[hour],m:[minute]
                            })
                        }
                        S.push({
                            mode: pushlist[j].mode,
                            username: this.app.list[i].user.username,
                            id: pushlist[j].id,
                            schedule : {
                                schedules: schedules
                            }
                        })

                    }
                }
            }
            console.log(S)
            doSetSchedule = (i) => {
                later.setInterval (()=>{
                    this.callPush(S[i])
                }, S[i].schedule);
            }
            doTimeout = (i) => {
                var now = moment()
                if(now.isAfter(S[i].nextTime)) {
                    this.callPush(S[i])
                } else {
                    setTimeout(() => {
                        this.callPush(S[i])
                    },now.diff(S[i].nextTime) * -1)
                }
            }
            for(var i = 0; i < S.length; i++) {
                if(S[i].mode == 1) {
                    doSetSchedule(i)
                } else {
                    doTimeout(i)
                }
            }

        },
        callPush: function(obj) {
            console.log(obj)
            for(var i = 0; i < this.browserSync.length; i++) {
                if(this.browserSync[i].username == obj.username) {
                    this.browserSync[i].win.webContents.send (con.IPC.PUSH_PRODUCT_REQUEST, obj)
                    return
                }
            }
        },
        listenSync: function(){
            ipcRenderer.on (con.IPC.SYNC_SUCCESS, (event, data) => {
                for(var i = 0; i < this.app.list.length; i++) {
                    if(this.app.list[i].user.username == data.user.username) {
                        var pushArr = this.app.list[i].pushArr
                        this.app.list[i] = data
                        this.app.list[i].pushArr = pushArr
                        this.app.list.push(data)
                        this.app.list.pop()
                        ls.set(con.LS.LIST, JSON.stringify(this.app.list))
                    }
                }
                if(this.app.begin == this.app.list.length) {
                    this.schedulePush()
                }
                this.app.begin++
            })
            ipcRenderer.on (con.IPC.SYNC_FAIL, (event, data) => {
                for(var i = 0; i < this.app.list.length; i++) {
                    if(this.app.list[i].partition == data) {
                        var username = this.app.list[i].user.username

                        this.app.list[i].disconnected = true
                        this.app.list.push({})
                        this.app.list.pop()
                        var opt = {
                            filename: con.SHOPEE.KENH_NGUOI_BAN,
                            isShow: con.WINDOW.USER_SYNC.SHOW,
                            user: this.app.list[i].partition,
                            width: con.WINDOW.USER_SYNC.width,
                            height: con.WINDOW.USER_SYNC.height,
                            preload: con.GO.SYNC,
                            parent: true
                        }
                        for(var j = 0; j < this.browserSync.length; j++) {
                            if(this.browserSync[j].username == username) {
                                this.browserSync.splice(j, 1)
                            }
                        }
                        main.openWindow(opt, (win) => {
                            this.browserSync.push({
                                username: username,
                                win: win
                            })
                        })
                    }
                }
            })
            ipcRenderer.on (con.IPC.SYNC_NOT_LOGIN, (event, data) => {
                for(var i = 0; i < this.app.list.length; i++) {
                    if(this.app.list[i].partition == data) {
                        this.app.list[i].notLogin = true
                        this.app.list.push({})
                        this.app.list.pop()
                    }
                }
            })
        },
        listenPush: function(){
            ipcRenderer.on (con.IPC.PUSH_READY_TO_USE, (event, data) => {
                this.pushP.searchLoading = false
                this.callSearchPush()
            })
            ipcRenderer.on (con.IPC.PUSH_SEARCH_RESUTL, (event, data) => {
                console.log(data)
                this.pushP.searchLoading = false
                this.pushP.searchTotal = data.meta.total
                this.pushP.searchList = data.products
            })
            ipcRenderer.on (con.IPC.SYNC_REQUEST_PUSH_SUCCESS, (event, data) => {
                console.log(data)
                var doTimeout = (obj) => {
                    setTimeout(() => {
                        this.callPush({
                            mode: obj.mode,
                            username: obj.username,
                            id: obj.id,
                            nextTime: obj.nextTime
                        })
                    },moment().diff(obj.nextTime) * -1)
                }
                var newNextTime = moment().add(data.res.data.product.cooldown_seconds, 'seconds').format()
                console.log(newNextTime)
                for(var i = 0; i < this.app.list.length; i++) {
                    if(this.app.list[i].user.username == data.detech.username) {
                        var listpush = this.app.list[i].pushArr
                        for(var j = 0; j < listpush.length; j++) {
                            if(listpush[j].id == data.detech.id){
                                listpush[j].nextTime = newNextTime
                                ls.set(con.LS.LIST, this.app.list)
                                if(listpush[j].mode == 0) {
                                    doTimeout({
                                        mode: listpush[j].mode,
                                        username: data.detech.username,
                                        id: data.detech.id,
                                        nextTime: newNextTime
                                    })
                                }
                                break
                            }
                        }
                        break
                    }
                }
            })
        },
        listenGo: function(){
            ipcRenderer.on (con.IPC.GO_CLOSE, (event, data) => {
                remote.getGlobal ('index_window').show()
            })
        },
        syncInfo: function(){
            var openBrowser = (i) => {
                var opt = {
                    filename: con.SHOPEE.KENH_NGUOI_BAN,
                    isShow: false,
                    user: this.app.list[i].partition,
                    width: con.WINDOW.USER_SYNC.width,
                    height: con.WINDOW.USER_SYNC.height,
                    preload: con.GO.SYNC,
                    parent: true
                }
                main.openWindow(opt, (win)=>{
                    this.browserSync.push({
                        username: this.app.list[i].user.username,
                        win: win
                    })
                })
            }
            
            for(var i = 0; i < this.app.list.length; i++) {
            	console.log(this.app.list[i]);
                openBrowser(i)
            }
        },
        clickOpenModalRegister: function() {
        	 var $cloneModal = $('#modal-register')
             $cloneModal.modal('show');
		},
        //clone
        clickOpenClone: function (username) {
            var openClone = (indexOfWin) => {
                var $cloneModal = $('#modal-clone')
                $cloneModal.modal('show')
                this.app.clone.username = username
                this.browserSync[indexOfWin].win.webContents.send(con.IPC.GET_CLONE, {})
                ipcRenderer.on (con.IPC.CLONE_RESULT, (event, data) => {
                    console.log(data)
                    if(data.goUser == this.app.clone.username) {
                        this.app.clone.list = data.products
                        this.app.clone.itemModels = data['item-models']
                    }
                })
            }
            for(var i = 0; i < this.browserSync.length; i++) {
                if(this.browserSync[i].username == username) {
                    openClone(i)
                    break
                }
            }
        },
        clickCheckAllClone: function () {
            this.app.clone.isCheckAll = !this.app.clone.isCheckAll
            for(var i = 0 ; i < this.app.clone.list.length; i++) {
                this.app.clone.list[i].pick =this.app.clone.isCheckAll
            }
        },
        clickExportExcelClone: function () {
            var table = []
            for(var i = 0; i < this.app.clone.list.length; i++) {
                var product = this.app.clone.list[i]
                var items = []
                for(var j = 0; j < this.app.clone.itemModels.length; j++) {
                    for(var k = 0; k < product.itemmodels.length; k++) {
                        if(product.itemmodels[k] == this.app.clone.itemModels[j].id) {
                            items.push(this.app.clone.itemModels[j])
                        }
                    }
                }
                var newPrice = parseInt(product.price) + this.app.clone.giachenhlech
                if(product.pick == true) {
                    table.push({
                        "ps_category_list_id": product.new_subcategories[0][2],
                        "ps_product_name": this.app.clone.tiento + product.name + this.app.clone.hauto,
                        "ps_product_description": product.description,
                        "ps_price": newPrice,
                        "ps_stock": product.stock,
                        "ps_product_weight": product.weight,
                        "ps_days_to_ship": product.estimated_days ? product.estimated_days : "",
                        "ps_sku_ref_no_parent": product.parent_sku ? product.parent_sku : "",
                        "ps_mass_upload_variation_help": "",
                        "ps_variation 1 ps_variation_sku": items[0] ? items[0].sku : "",
                        "ps_variation 1 ps_variation_name": items[0] ? items[0].name : "",
                        "ps_variation 1 ps_variation_price": items[0] ? (parseInt(items[0].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 1 ps_variation_stock": items[0] ? items[0].stock : "",


                        "ps_variation 2 ps_variation_sku": items[1] ? items[1].sku : "",
                        "ps_variation 2 ps_variation_name": items[1] ? items[1].name : "",
                        "ps_variation 2 ps_variation_price": items[1] ? (parseInt(items[1].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 2 ps_variation_stock": items[1] ? items[1].stock : "",

                        "ps_variation 3 ps_variation_sku": items[2] ? items[2].sku : "",
                        "ps_variation 3 ps_variation_name": items[2] ? items[2].name : "",
                        "ps_variation 3 ps_variation_price": items[2] ? (parseInt(items[2].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 3 ps_variation_stock": items[2] ? items[2].stock : "",

                        "ps_variation 4 ps_variation_sku": items[3] ? items[3].sku : "",
                        "ps_variation 4 ps_variation_name": items[3] ? items[3].name : "",
                        "ps_variation 4 ps_variation_price": items[3] ? (parseInt(items[3].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 4 ps_variation_stock": items[3] ? items[3].stock : "",

                        "ps_variation 5 ps_variation_sku": items[4] ? items[4].sku : "",
                        "ps_variation 5 ps_variation_name": items[4] ? items[4].name : "",
                        "ps_variation 5 ps_variation_price": items[4] ? (parseInt(items[4].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 5 ps_variation_stock": items[4] ? items[4].stock : "",

                        "ps_variation 6 ps_variation_sku": items[5] ? items[5].sku : "",
                        "ps_variation 6 ps_variation_name": items[5] ? items[5].name : "",
                        "ps_variation 6 ps_variation_price": items[5] ? (parseInt(items[5].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 6 ps_variation_stock": items[5] ? items[5].stock : "",

                        "ps_variation 7 ps_variation_sku": items[6] ? items[6].sku : "",
                        "ps_variation 7 ps_variation_name": items[6] ? items[6].name : "",
                        "ps_variation 7 ps_variation_price": items[6] ? (parseInt(items[6].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 7 ps_variation_stock": items[6] ? items[6].stock : "",

                        "ps_variation 8 ps_variation_sku": items[7] ? items[7].sku : "",
                        "ps_variation 8 ps_variation_name": items[7] ? items[7].name : "",
                        "ps_variation 8 ps_variation_price": items[7] ? (parseInt(items[7].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 8 ps_variation_stock": items[7] ? items[7].stock : "",

                        "ps_variation 9 ps_variation_sku": items[8] ? items[8].sku : "",
                        "ps_variation 9 ps_variation_name": items[8] ? items[8].name : "",
                        "ps_variation 9 ps_variation_price": items[8] ? (parseInt(items[8].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 9 ps_variation_stock": items[8] ? items[8].stock : "",

                        "ps_variation 10 ps_variation_sku": items[9] ? items[9].sku : "",
                        "ps_variation 10 ps_variation_name": items[9] ? items[9].name : "",
                        "ps_variation 10 ps_variation_price": items[9] ? (parseInt(items[9].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 10 ps_variation_stock": items[9] ? items[9].stock : "",

                        "ps_variation 11 ps_variation_sku": items[10] ? items[10].sku : "",
                        "ps_variation 11 ps_variation_name": items[10] ? items[10].name : "",
                        "ps_variation 11 ps_variation_price": items[10] ? (parseInt(items[10].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 11 ps_variation_stock": items[10] ? items[10].stock : "",

                        "ps_variation 12 ps_variation_sku": items[11] ? items[11].sku : "",
                        "ps_variation 12 ps_variation_name": items[11] ? items[11].name : "",
                        "ps_variation 12 ps_variation_price": items[11] ? (parseInt(items[11].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 12 ps_variation_stock": items[11] ? items[11].stock : "",

                        "ps_variation 13 ps_variation_sku": items[12] ? items[12].sku : "",
                        "ps_variation 13 ps_variation_name": items[12] ? items[12].name : "",
                        "ps_variation 13 ps_variation_price": items[12] ? (parseInt(items[12].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 13 ps_variation_stock": items[12] ? items[12].stock : "",

                        "ps_variation 14 ps_variation_sku": items[13] ? items[13].sku : "",
                        "ps_variation 14 ps_variation_name": items[13] ? items[13].name : "",
                        "ps_variation 14 ps_variation_price": items[13] ? (parseInt(items[13].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 14 ps_variation_stock": items[13] ? items[13].stock : "",

                        "ps_variation 15 ps_variation_sku": items[14] ? items[14].sku : "",
                        "ps_variation 15 ps_variation_name": items[14] ? items[14].name : "",
                        "ps_variation 15 ps_variation_price": items[14] ? (parseInt(items[14].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 15 ps_variation_stock": items[14] ? items[14].stock : "",

                        "ps_variation 16 ps_variation_sku": items[15] ? items[15].sku : "",
                        "ps_variation 16 ps_variation_name": items[15] ? items[15].name : "",
                        "ps_variation 16 ps_variation_price": items[15] ? (parseInt(items[15].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 16 ps_variation_stock": items[15] ? items[15].stock : "",

                        "ps_variation 17 ps_variation_sku": items[16] ? items[16].sku : "",
                        "ps_variation 17 ps_variation_name": items[16] ? items[16].name : "",
                        "ps_variation 17 ps_variation_price": items[16] ? (parseInt(items[16].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 17 ps_variation_stock": items[16] ? items[16].stock : "",

                        "ps_variation 18 ps_variation_sku": items[17] ? items[17].sku : "",
                        "ps_variation 18 ps_variation_name": items[17] ? items[17].name : "",
                        "ps_variation 18 ps_variation_price": items[17] ? (parseInt(items[17].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 18 ps_variation_stock": items[17] ? items[17].stock : "",

                        "ps_variation 19 ps_variation_sku": items[18] ? items[18].sku : "",
                        "ps_variation 19 ps_variation_name": items[18] ? items[18].name : "",
                        "ps_variation 19 ps_variation_price": items[18] ? (parseInt(items[18].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 19 ps_variation_stock": items[18] ? items[18].stock : "",

                        "ps_variation 20 ps_variation_sku": items[19] ? items[19].sku : "",
                        "ps_variation 20 ps_variation_name": items[19] ? items[19].name : "",
                        "ps_variation 20 ps_variation_price": items[19] ? (parseInt(items[19].price) + this.app.clone.giachenhlech) : "",
                        "ps_variation 20 ps_variation_stock": items[19] ? items[19].stock : "",

                        "ps_img_1": product.images[0] ? "https://cf.shopee.vn/file/" + product.images[0] : "",
                        "ps_img_2": product.images[1] ? "https://cf.shopee.vn/file/" + product.images[1] : "",
                        "ps_img_3": product.images[2] ? "https://cf.shopee.vn/file/" + product.images[2] : "",
                        "ps_img_4": product.images[3] ? "https://cf.shopee.vn/file/" + product.images[3] : "",
                        "ps_img_5": product.images[4] ? "https://cf.shopee.vn/file/" + product.images[4] : "",
                        "ps_img_6": product.images[5] ? "https://cf.shopee.vn/file/" + product.images[5] : "",
                        "ps_img_7": product.images[6] ? "https://cf.shopee.vn/file/" + product.images[6] : "",
                        "ps_img_8": product.images[7] ? "https://cf.shopee.vn/file/" + product.images[7] : "",
                        "ps_img_9": product.images[8] ? "https://cf.shopee.vn/file/" + product.images[8] : "",
                        "ps_mass_upload_shipment_help": "",
                        "channel 50012 switch": "Mở",
                        "channel 50011 switch": "Mở",
                        "channel 50016 switch": "Mở",
                        "channel 50015 switch": "Mở",
                        "channel 50010 switch": "Mở"
                    })
                }
            }
            if(table.length == 0) return this.handleErr(con.ERR.DOWNLOAD_EXCEL_EMPTY)
            var ws = XLSX.utils.json_to_sheet(table)
            var wb = XLSX.utils.book_new()
            XLSX.utils.book_append_sheet(wb, ws, "Presidents")
            console.log(wb)
            XLSX.writeFile(wb,main.getPathDownload() + '/' + table.length + '_sp_' + this.app.clone.username + '_' + new Date().getTime() +'.xlsx');
            utils.alert({title: con.NOTI.DEFAULT_NOTI, msg: con.NOTI.DOWNLOAD_EXCEL_SUCCESS})
        },
        clickCloseClone: function () {
            var $cloneModal = $('#modal-clone')
            $cloneModal.modal('hide')
            this.app.clone = {
                list:[],
                itemModels: [],
                search: '',
                tiento:'',
                hauto:'',
                giachenhlech:'',
                isCheckAll: false,
                username:''
            }
        }

    }
})
window.onbeforeunload = (e) => {
    var allWindows = remote.getGlobal ('shopee_window')
    console.log(ls.get(con.LS.LIST));
    var list = ls.get(con.LS.LIST) ? JSON.parse(ls.get(con.LS.LIST)) : []
    for(var i = 0; i < list.length; i++) {
        list[i].disconnected = true
    }
    ls.set(con.LS.LIST, JSON.stringify(list))
    ls.set(con.LS.LAST_USE, new Date().getTime())
    main.deleteFolderPrint()

    //delete partion
    var listPartions = []
    for(var i = 0; i < list.length; i++) {
        listPartions.push(list[i].partition)
    }
    main.deletePartionFolder(listPartions)

    for (var i = 0; i < allWindows.length; i++) {
        allWindows[i].destroy()
    }
}


